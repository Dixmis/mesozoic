﻿using System;

namespace mesozoic_console
{
    public class Program
    {
        static void Main(string[] args)
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Dinosaur pierre = new Dinosaur("Pierre", "Rocher", 1);
            Dinosaur michel = new Dinosaur("Michel", "Tyrannosaure", 7);
            Console.WriteLine(louis.sayHello());
            Console.WriteLine(nessie.sayHello());
            Console.WriteLine(pierre.sayHello());
            Console.WriteLine(michel.sayHello());
            Console.WriteLine(louis.hug(pierre));
            Console.WriteLine(nessie.hug(michel));
            Console.WriteLine(michel.roar());
            Console.WriteLine(nessie.hug(pierre));
        }
    }
}
